-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 14-02-2022 a las 05:19:07
-- Versión del servidor: 10.4.21-MariaDB
-- Versión de PHP: 8.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `studentsregister`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `control_register`
--

CREATE TABLE `control_register` (
  `id` int(11) NOT NULL,
  `description` varchar(120) COLLATE utf8_bin NOT NULL,
  `id_intern` int(11) NOT NULL,
  `id_manager` int(11) NOT NULL,
  `fecha_inicio` text COLLATE utf8_bin NOT NULL,
  `fecha_final` text COLLATE utf8_bin NOT NULL,
  `horario` text COLLATE utf8_bin NOT NULL,
  `horas_practica` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `control_register`
--

INSERT INTO `control_register` (`id`, `description`, `id_intern`, `id_manager`, `fecha_inicio`, `fecha_final`, `horario`, `horas_practica`, `created_at`, `updated_at`) VALUES
(6, 'informe segundo', 16, 7, '05/06/2020', '02/02/2022', '12:00 am a 3:00 pm', 12, '2022-02-14 03:27:58', '2022-02-14 04:16:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `evaluation`
--

CREATE TABLE `evaluation` (
  `id` int(11) NOT NULL,
  `id_intern` int(11) NOT NULL,
  `description` text COLLATE utf8_bin NOT NULL,
  `status` int(11) NOT NULL,
  `correction` text COLLATE utf8_bin NOT NULL,
  `grade` decimal(10,0) NOT NULL,
  `file` text COLLATE utf8_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `evaluation`
--

INSERT INTO `evaluation` (`id`, `id_intern`, `description`, `status`, `correction`, `grade`, `file`, `created_at`, `updated_at`) VALUES
(14, 15, 'informe segundo', 1, 'Culminado', '7', 'descipcion - copia.pdf', '2022-02-11 17:19:36', '2022-02-11 17:20:17');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `intern`
--

CREATE TABLE `intern` (
  `id` int(11) NOT NULL,
  `name` varchar(90) COLLATE utf8_bin NOT NULL,
  `lastname` varchar(120) COLLATE utf8_bin NOT NULL,
  `dni` varchar(120) COLLATE utf8_bin NOT NULL,
  `phone` text COLLATE utf8_bin DEFAULT NULL,
  `email` text COLLATE utf8_bin NOT NULL,
  `place_residence` text COLLATE utf8_bin DEFAULT NULL,
  `career` text COLLATE utf8_bin NOT NULL,
  `status` int(11) NOT NULL,
  `university` text COLLATE utf8_bin NOT NULL,
  `institution_name` text COLLATE utf8_bin NOT NULL,
  `manager_id` int(11) NOT NULL,
  `internship_month` varchar(50) COLLATE utf8_bin NOT NULL,
  `number_months_internship` int(11) NOT NULL,
  `internship_application` text COLLATE utf8_bin DEFAULT NULL,
  `curriculum_vitae` text COLLATE utf8_bin DEFAULT NULL,
  `covid19_exam` text COLLATE utf8_bin DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `intern`
--

INSERT INTO `intern` (`id`, `name`, `lastname`, `dni`, `phone`, `email`, `place_residence`, `career`, `status`, `university`, `institution_name`, `manager_id`, `internship_month`, `number_months_internship`, `internship_application`, `curriculum_vitae`, `covid19_exam`, `created_at`, `updated_at`) VALUES
(15, 'Carlos', 'Rivera', '0564212312321', '89456732', 'carlos20@gmail.com', 'SPS', 'Industrial', 1, 'Universidad', 'Universidad', 6, 'Febrero', 3, NULL, NULL, NULL, '2022-02-04 03:42:55', '2022-02-04 03:42:55'),
(16, 'Leo', 'Ramirez', '0501199500054', '97502312', 'hola@gmail.com', 'SPS', 'sistemas', 1, 'Universidad', 'Universidad de los Santos', 7, 'descipcion - copia.pdf', 8, 'descipcion - copia (2).pdf', 'descipcion.pdf', 'descipcion - copia.pdf', '2022-02-14 04:03:22', '2022-02-14 04:03:22');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `management`
--

CREATE TABLE `management` (
  `id` int(11) NOT NULL,
  `management_name` varchar(120) COLLATE utf8_bin NOT NULL,
  `manager` varchar(120) COLLATE utf8_bin NOT NULL,
  `vacancy` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `management`
--

INSERT INTO `management` (`id`, `management_name`, `manager`, `vacancy`, `created_at`, `updated_at`) VALUES
(6, 'Wendy', 'Industrial', 1, '2022-01-13 04:10:29', '2022-02-04 03:42:55'),
(7, 'Katia', 'Logistica', 3, '2022-01-13 04:10:51', '2022-02-14 04:03:22');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `process`
--

CREATE TABLE `process` (
  `id_user` int(11) NOT NULL,
  `id_manager` int(11) NOT NULL,
  `process_name` text COLLATE utf8_bin NOT NULL,
  `status` int(11) NOT NULL,
  `description` text COLLATE utf8_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(11) NOT NULL,
  `dni` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `phone` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `photo_profile` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `address` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `role`, `dni`, `phone`, `photo_profile`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `address`) VALUES
(1, 'Prueba', 0, '', '', '', 'prueba@gmail.com', NULL, '$2y$10$KqFoJQZas02khFwxjC1ec.PhmZC2sQtRYBYKMvPP3/GMFKbnFyAu6', NULL, '2021-11-10 10:22:45', '2021-11-10 10:22:45', '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `control_register`
--
ALTER TABLE `control_register`
  ADD PRIMARY KEY (`id`),
  ADD KEY `control_register_ibfk_1` (`id_intern`),
  ADD KEY `id_manager` (`id_manager`);

--
-- Indices de la tabla `evaluation`
--
ALTER TABLE `evaluation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_intern` (`id_intern`);

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indices de la tabla `intern`
--
ALTER TABLE `intern`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `management`
--
ALTER TABLE `management`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indices de la tabla `process`
--
ALTER TABLE `process`
  ADD PRIMARY KEY (`id_user`,`id_manager`),
  ADD KEY `id_manager` (`id_manager`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `control_register`
--
ALTER TABLE `control_register`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `evaluation`
--
ALTER TABLE `evaluation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `intern`
--
ALTER TABLE `intern`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `management`
--
ALTER TABLE `management`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `control_register`
--
ALTER TABLE `control_register`
  ADD CONSTRAINT `control_register_ibfk_1` FOREIGN KEY (`id_intern`) REFERENCES `intern` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `control_register_ibfk_2` FOREIGN KEY (`id_manager`) REFERENCES `management` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `evaluation`
--
ALTER TABLE `evaluation`
  ADD CONSTRAINT `evaluation_ibfk_1` FOREIGN KEY (`id_intern`) REFERENCES `intern` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `process`
--
ALTER TABLE `process`
  ADD CONSTRAINT `process_ibfk_1` FOREIGN KEY (`id_manager`) REFERENCES `management` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `process_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `intern` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
