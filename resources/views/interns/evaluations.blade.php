{{-- Aqui se coloca la plantilla del panel de administrador --}}
@extends('layouts.main')

{{-- Aqui se coloca el titulo de esta pagina que se esta desarroollando --}}
@section('title', 'Documentacion y Evaluacion - Administración')

{{-- Aqui la seccion que trae el contenido centrada a la plantilla --}}
@section('section')
<div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">
                Administraci&oacute;n para Documentacion y Evaluaci&oacute;n de Practicantes
            </h3>
            <a type="button" class="btn btn-success float-right" href="/intern/mostrar_evaluacion/create/{{$id_intern}}"><i class="fas fa-plus"></i> Agregar</a>
            <a type="button" class="btn btn-primary float-right" href="/intern"><i class="fas fa-arrow-left"></i> Regresar</a>
          
         </div>
         
          <!-- /.card-header -->
          {{-- Lista de Gerencias --}}
          <div class="card-body">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h5><i class="icon fas fa-check"></i> Alerta!</h5>
              <p>{{ $message }}</p>
            </div>
            @endif
            <table id="example1" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>Documentos</th>
                <th>Estado</th>
                <th>Correcciones</th>
                <th>Nota</th>
                <th>Acciones</th>
              </tr>
              </thead>
              <tbody>
                @foreach ($evaluations as $evaluation)
                <tr>
                <td>{{$evaluation->description}}
                <a href="/pdf/evaluaciones/{{$evaluation->file}}" target="_blank"> Ver</a>
                </td>
                <td>
                  @if ($evaluation->status==1)
                    <button  class="btn btn-success">Aprobado</button>   
                  @else
                    <button  class="btn btn-danger">Reprobado</button>    
                  @endif
                </td>
                <td>
                  @if ($evaluation->correction=='Culminado')
                    <button  class="btn btn-success">Culminado</button>   
                  @else
                    <button  class="btn btn-primary">Pendiente</button>    
                  @endif
                </td>
                <td>
                  {{$evaluation->grade}}
                </td>
                <td><div class="btn-group">
                    <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" data-offset="-52" aria-expanded="true">
                      <i class="fas fa-bars"></i>
                    </button>
                    <div class="dropdown-menu" role="menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-52px, 31px, 0px);">
                        <form action="{{ url('/intern/mostrar_evaluacion/'.$evaluation->id.'/destroy') }}" method="Post">
                          @csrf
                          <a href="{{ url('/intern/mostrar_evaluacion/'.$id_intern.'/edit/'.$evaluation->id) }}" class="dropdown-item"> <i class="fas fa-bars"></i> Editar</a>
                      
                          <button  type="submit" class="dropdown-item"> <i class="fas fa-bars"></i> Eliminar</button> 
                        </form>
                       
                     </div>
                  </div>
                </td>
              </tr>
                @endforeach
               <tr>
                <td> - </td>
                <td> - </td>
                <td> - </td>
                <td><strong> Nota Promedio: {{$promgradeevaluation}}</strong></td>
                <td> - </td>
               </tr>
              </tbody>
              <tfoot>
              <tr>
                <th>Documentos</th>
                <th>Estado</th>
                <th>Correcciones</th>
                <th>Nota</th>
                <th>Acciones</th>
              </tr>
              </tfoot>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
@endsection


