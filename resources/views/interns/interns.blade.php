{{-- Aqui se coloca la plantilla del panel de administrador --}}
@extends('layouts.main')

{{-- Aqui se coloca el titulo de esta pagina que se esta desarroollando --}}
@section('title', 'Evaluacion para Practicantes - Administración')

{{-- Aqui la seccion que trae el contenido centrada a la plantilla --}}
@section('section')
<div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">
                Administraci&oacute;n para Evaluaci&oacute;n de Practicantes
            </h3>
         </div>
          <!-- /.card-header -->
          {{-- Lista de Gerencias --}}
          <div class="card-body">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h5><i class="icon fas fa-check"></i> Alerta!</h5>
              <p>{{ $message }}</p>
            </div>
            @endif
            <table id="example1" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>Nombre de Practicante</th>
                <th>Carrera</th>
                <th>Nombre de la Instituci&oacute;n</th>
                <th>Acciones</th>
              </tr>
              </thead>
              <tbody>
                @foreach ($interns as $intern)
                @if ($intern->status == 2)
                <tr>
                <td>{{$intern->name}} {{$intern->lastname}} 
                </td>
                <td>{{$intern->career}}
                </td>
                <td>{{$intern->institution_name}} 
                </td>
                <td><div class="btn-group">
                     <a href="/intern/mostrar_evaluacion/{{$intern->id}}" type="button" class="btn btn-primary"><i class="far fa-eye"></i> Evaluar</a>     
                  </div></td>
                </tr>
                @endif
                @endforeach
              </tbody>
              <tfoot>
              <tr>
                <th>Nombre de Practicante</th>
                <th>Carrera</th>
                <th>Nombre de la Instituci&oacute;n</th>
                <th>Acciones</th>
              </tr>
              </tfoot>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
@endsection


