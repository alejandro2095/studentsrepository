{{-- Aqui se coloca la plantilla del panel de administrador --}}
@extends('layouts.register_intern')

{{-- Aqui se coloca el titulo de esta pagina que se esta desarroollando --}}
@section('title', 'Registrarse')

{{-- Aqui la seccion que trae el contenido centrada a la plantilla --}}
@section('section')

<div class="container-fluid">
    <div class="row">
      {{-- Aqui se hace una condicion donde si en las gerencias no hay ninguna vacante entonces tirara el mensaje de que no puede registrarse y si hay vacantes podra acceder al formulario --}}
      @if ($count_manager==0)
      <div class="col-12">
          <div class="row">   
            <div class="col-12">
              <h4><b>Lamentamos informar que no se encuentran vacantes en ningun área para poder aplicar en estos momentos.</b></h4>
              @if ($message = Session::get('success'))
            <div class="alert alert-success alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h5><i class="icon fas fa-check"></i> Alerta!</h5>
              <p>{{ $message }}</p>
            </div>
            @endif
            </div>
          </div> 
        @else
        <div class="col-12">
          <p class="login-box-msg text-left"><span class="text-danger">*</span>Registrese para poder aplicar a un area, los campos que contengan este asterisco <span class="text-danger">*</span> significan que son obligatorios de llenar.</p>
          <div class="card-header mb-3">
            <h4><b>Información Personal</b></h4>
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h5><i class="icon fas fa-check"></i> Alerta!</h5>
              <p>{{ $message }}</p>
            </div>
            @endif
            @if ($message = Session::get('errores'))
            <div class="alert alert-danger alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h5><i class="icon fas fa-check"></i> Alerta!</h5>
              <p>{{ $message }}</p>
            </div>
            @endif
          </div>
          <form action="{{ route('intern.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">   
              <div class="col-6">
                <div class="form-group">
                  <label>Nombre<span class="text-danger">*</span></label>
                  <input type="text" class="form-control" value="{{old('name')}}" id="name" name="name">
                  @error('name')
                  <div class="alert alert-warning alert-dismissible mt-2">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h5><i class="icon fas fa-exclamation-triangle"></i> Alerta!</h5>
                    {{ $message }}
                  </div>
                  @enderror      
                </div>
              </div>
              <div class="col-6">
                <div class="form-group">
                  <label>Apellido<span class="text-danger">*</span></label>
                  <input type="text" class="form-control" value="{{old('lastname')}}" id="lastname" name="lastname">
                  @error('lastname')
                  <div class="alert alert-warning alert-dismissible mt-2">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h5><i class="icon fas fa-exclamation-triangle"></i> Alerta!</h5>
                    {{ $message }}
                  </div>
                  @enderror      
                </div>
              </div>
              <div class="col-md-6 col-lg-4">
                <div class="form-group">
                  <label>DNI<span class="text-danger">*</span></label>
                  <input type="text" class="form-control" value="{{old('dni')}}" id="dni" name="dni" maxlength="13">
                  @error('dni')
                  <div class="alert alert-warning alert-dismissible mt-2">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h5><i class="icon fas fa-exclamation-triangle"></i> Alerta!</h5>
                    {{ $message }}
                  </div>
                  @enderror      
                </div>
              </div>
              <div class="col-md-6 col-lg-4">
                <div class="form-group">
                  <label>Teléfono</label>
                  <input type="number" class="form-control" value="{{old('phone')}}" id="phone" name="phone">
                </div>
              </div>
              <div class="col-md-6 col-lg-4">
                <div class="form-group">
                  <label>Correo electrónico<span class="text-danger">*</span></label>
                  <input type="email" class="form-control" value="{{old('email')}}"  id="email" name="email">
                  @error('email')
                  <div class="alert alert-warning alert-dismissible mt-2">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h5><i class="icon fas fa-exclamation-triangle"></i> Alerta!</h5>
                    {{ $message }}
                  </div>
                  @enderror      
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                    <label>¿Gerencia a la cual desea aplicar?</label>
                    <select class="form-control" id="manager_id" name="manager_id">
                      @foreach ($managers as $manager)
                       <option value="{{$manager->id}}">{{$manager->manager}}</option>
                      @endforeach
                    </select>    
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Lugar de residencia</label>
                  <input type="text" class="form-control" value="{{old('place_residence')}}" id="place_residence" name="place_residence"> 
                </div>
              </div>
              <div class="col-12 card-header mb-3">
                <h4><b>Información Académica</b></h4>
              </div>
              <div class="col-6">
                <div class="form-group">
                  <label>Carrera<span class="text-danger">*</span></label>
                  <input type="text" class="form-control" value="{{old('career')}}" id="career" name="career">
                  @error('career')
                  <div class="alert alert-warning alert-dismissible mt-2">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h5><i class="icon fas fa-exclamation-triangle"></i> Alerta!</h5>
                    {{ $message }}
                  </div>
                  @enderror      
                </div>
              </div>
              <div class="col-6">
                <div class="form-group">
                    <label>¿Es Colegio o Universidad?</label>
                    <select class="form-control" value="{{old('university')}}" id="university" name="university">
                      <option>Colegio</option>
                      <option>Universidad</option>
                    </select>    
                </div>
              </div>
              <div class="col-6">
                <div class="form-group">
                  <label>Nombre de la institución<span class="text-danger">*</span></label>
                  <input type="text" class="form-control" value="{{old('institution_name')}}" id="institution_name" name="institution_name">
                  @error('institution_name')
                  <div class="alert alert-warning alert-dismissible mt-2">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h5><i class="icon fas fa-exclamation-triangle"></i> Alerta!</h5>
                    {{ $message }}
                  </div>
                  @enderror      
                </div>
              </div>
              <div class="col-6">
                <div class="form-group">
                    <label>¿Mes en que hará la practica?</label>
                    <select class="form-control"  id="internship_month" name="internship_month">
                      <option value="1">Enero</option>
                      <option value="2">Febrero</option>
                      <option value="3">Marzo</option>
                      <option value="4">Abril</option>
                      <option value="5">Mayo</option>
                      <option value="6">Junio</option>
                      <option value="7">Julio</option>
                      <option value="8">Agosto</option>
                      <option value="9">Septiembre</option>
                      <option value="10">Octubre</option>
                      <option value="11">Noviembre</option>
                      <option value="12">Diciembre</option>
                    </select>    
                </div>
              </div>
              <div class="col-md-6 col-lg-4">
                <div class="form-group">
                  <label>¿Cuantos meses durara la práctica profesional?<span class="text-danger">*</span></label>
                  <input type="number" class="form-control" value="{{old('number_months_internship')}}" id="number_months_internship" name="number_months_internship">
                  @error('number_months_internship')
                  <div class="alert alert-warning alert-dismissible mt-2">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h5><i class="icon fas fa-exclamation-triangle"></i> Alerta!</h5>
                    {{ $message }}
                  </div>
                  @enderror      
                </div>
              </div>
              <div class="col-12 card-header mb-3">
                <h4><b>Documentos</b></h4>
              </div>
              <div class="col-md-6 col-lg-4">
                <div class="form-group">
                  <label for="customFile">Solicitud de práctica</label>
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="internship_application" name="internship_application">
                    <label class="custom-file-label" for="customFile">Escoje el archivo</label>
                    @error('internship_application')
                    <div class="alert alert-warning alert-dismissible mt-2">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <h5><i class="icon fas fa-exclamation-triangle"></i> Alerta!</h5>
                      @php
                          echo html_entity_decode($message)
                      @endphp
                    </div>
                    @enderror 
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-lg-4">
                <div class="form-group">
                  <label for="customFile">Hoja de Vida</label>
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="curriculum_vitae" name="curriculum_vitae">
                    <label class="custom-file-label" for="customFile">Escoje el archivo</label>
                    @error('curriculum_vitae')
                    <div class="alert alert-warning alert-dismissible mt-2">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <h5><i class="icon fas fa-exclamation-triangle"></i> Alerta!</h5>
                      @php
                          echo html_entity_decode($message)
                      @endphp
                    </div>
                    @enderror 
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-lg-4">
                <div class="form-group">
                  <label for="customFile">Examen de Covid 19</label>
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="covid19_exam" name="covid19_exam">
                    <label class="custom-file-label" for="customFile">Escoje el archivo</label>
                    @error('covid19_exam')
                    <div class="alert alert-warning alert-dismissible mt-2">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <h5><i class="icon fas fa-exclamation-triangle"></i> Alerta!</h5>
                      @php
                          echo html_entity_decode($message)
                      @endphp
                    </div>
                    @enderror 
                  </div>
                </div>
              </div>
             
            </div>
           <button  type="submit" class="btn btn-success">Enviar</button>
          </form>
  
  
      
          <!-- /.card -->
        </div> 
      @endif
    
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>

@endsection
