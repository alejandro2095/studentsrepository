{{-- Aqui se coloca la plantilla del panel de administrador --}}
@extends('layouts.main')

{{-- Aqui se coloca el titulo de esta pagina que se esta desarroollando --}}
@section('title', 'Editar Gerencia - Administración')

{{-- Aqui la seccion que trae el contenido centrada a la plantilla --}}
@section('section')
<div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">
                Editar Gerencia
            </h3>
            <a type="button" class="btn btn-primary float-right" href="{{ route('manager.index') }}"> Regresar</a>
          </div>
        
          <div class="card-body">
           {{-- Formulario Para guardar la Gerencia --}}
           <form action="{{ route('manager.update',$manager->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="row">   
          <div class="col-6">
            <div class="form-group">
              <label>Nombre Gerente</label>
              <input type="text" class="form-control" value="{{ $manager->management_name }}" id="management_name" name="management_name">
              @error('management_name')
              <div class="alert alert-warning alert-dismissible mt-2">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h5><i class="icon fas fa-exclamation-triangle"></i> Alerta!</h5>
                {{ $message }}
              </div>
              @enderror      
            </div>
          </div>
          <div class="col-6">
            <div class="form-group">
              <label>Gerencia</label>
              <input type="text" class="form-control" value="{{ $manager->manager }}" id="manager" name="manager">
              @error('manager')
              <div class="alert alert-warning alert-dismissible mt-2">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h5><i class="icon fas fa-exclamation-triangle"></i> Alerta!</h5>
                {{ $message }}
              </div>
              @enderror      
            </div>
          </div>
          <div class="col-6">
            <div class="form-group">
              <label>Vacante</label>
              <input type="number" class="form-control" value="{{ $manager->vacancy }}" id="vacancy" name="vacancy">      
              @error('vacancy')
              <div class="alert alert-warning alert-dismissible mt-2">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h5><i class="icon fas fa-exclamation-triangle"></i> Alerta!</h5>
                {{ $message }}
              </div>
              @enderror      
            </div>
          </div>
        
          </div>
          <button  type="submit" class="btn btn-success">Guardar</button>
          </form>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>

@endsection