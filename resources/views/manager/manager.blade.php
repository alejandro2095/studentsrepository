{{-- Aqui se coloca la plantilla del panel de administrador --}}
@extends('layouts.main')

{{-- Aqui se coloca el titulo de esta pagina que se esta desarroollando --}}
@section('title', 'Gerencias - Administración')

{{-- Aqui la seccion que trae el contenido centrada a la plantilla --}}
@section('section')
<div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">
                Administraci&oacute;n de Gerencias
            </h3>
            <a type="button" class="btn btn-success float-right" href="{{ route('manager.create') }}"><i class="fas fa-plus"></i> Agregar</a>
          </div>
         
          <!-- /.card-header -->
          {{-- Lista de Gerencias --}}
          <div class="card-body">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h5><i class="icon fas fa-check"></i> Alerta!</h5>
              <p>{{ $message }}</p>
            </div>
            @endif
            <table id="example1" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>Nombre Gerente</th>
                <th>Gerencia</th>
                <th>Vacante</th>
                <th>Acciones</th>
              </tr>
              </thead>
              <tbody>
                @foreach ($managers as $manager)
                <tr>
                <td>{{$manager->management_name}}
                </td>
                <td>{{$manager->manager}}
                </td>
                <td>{{$manager->vacancy}}
                </td>
                <td><div class="btn-group">
                    <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" data-offset="-52" aria-expanded="true">
                      <i class="fas fa-bars"></i>
                    </button>
                    <div class="dropdown-menu" role="menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-52px, 31px, 0px);">
                      <form action="{{ route('manager.destroy',$manager->id) }}" method="Post">
                        <a href="{{ route('manager.edit',$manager->id) }}" class="dropdown-item"> <i class="fas fa-bars"></i> Editar</a>
                        @csrf
                        @method('DELETE')
                        <button  type="submit" class="dropdown-item"> <i class="fas fa-bars"></i> Eliminar</button> 
                      </form>
                     </div>
                  </div></td>
              </tr>
                @endforeach
              </tbody>
              <tfoot>
              <tr>
                <th>Nombre Gerente</th>
                <th>Gerencia</th>
                <th>Vacante</th>
                <th>Acciones</th>
              </tr>
              </tfoot>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
@endsection


