{{-- Aqui se coloca la plantilla del panel de administrador --}}
@extends('layouts.main')

{{-- Aqui se coloca el titulo de esta pagina que se esta desarroollando --}}
@section('title', 'Usuarios - Administración')

{{-- Aqui la seccion que trae el contenido centrada a la plantilla --}}
@section('section')
<div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          @if ($message = Session::get('success'))
            <div class="alert alert-success alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h5><i class="icon fas fa-check"></i> Alerta!</h5>
              <p>{{ $message }}</p>
            </div>
          @endif
          <div class="card-header">
            <h3 class="card-title">
                Administraci&oacute;n de Usuarios
            </h3>
            <a type="button" class="btn btn-success float-right" href="{{ route('users.create') }}"><i class="fas fa-plus"></i> Agregar</a>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>#</th>
                <th>Nombre Completo</th>
                <th>DNI</th>
                <th>Tel&eacute;fono</th>
                <th>Correo electr&oacute;nico</th>
                <th>Acciones</th>
              </tr>
              </thead>
              <tbody>
                @foreach ($users as $user)
                <tr>
                <td>1</td>
                <td>{{$user->name}}
                </td>
                <td>{{$user->dni}}
                </td>
                <td>{{$user->phone}}
                </td>
                <td>{{$user->email}}
                </td>
               
                <td><div class="btn-group">
                    <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" data-offset="-52" aria-expanded="true">
                      <i class="fas fa-bars"></i>
                    </button>
                    <div class="dropdown-menu" role="menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-52px, 31px, 0px);">
                      <form action="{{ route('users.destroy',$user->id) }}" method="Post">
                        <a href="{{ route('users.edit',$user->id) }}" class="dropdown-item"> <i class="fas fa-bars"></i> Editar</a>
                        @csrf
                        @method('DELETE')
                        <button  type="submit" class="dropdown-item"> <i class="fas fa-bars"></i> Eliminar</button> 
                      </form>
                    </div>
                  </div></td>
              </tr>
                @endforeach
                
              
              </tbody>
              <tfoot>
              <tr>
                <th>#</th>
                <th>Nombre Completo</th>
                <th>DNI</th>
                <th>Tel&eacute;fono</th>
                <th>Correo electr&oacute;nico</th>
                <th>Acciones</th>
              </tr>
              </tfoot>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
@endsection


