@extends('layouts.main')

@section('title', 'Editar Usuario - Administración')

@section('section')
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">
            Editar Usuario
          </h3>
          <a type="button" class="btn btn-primary float-right" href="{{ route('users.index') }}"> Regresar</a>
        </div>
        <div class="card-body">
          <form action="{{ route('users.update',$user->id) }}" method="POST" enctype="multipart/form-data">
          @csrf
          @method('PUT')
            <div class="row">   
              <div class="col-6">
                <div class="form-group">
                  <label>Nombre completo</label>
                  <input type="text" class="form-control" id="user_name" name="user_name" value="{{ $user->name }}">
                  @error('user_name')
                  <div class="alert alert-warning alert-dismissible mt-2">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h5><i class="icon fas fa-exclamation-triangle"></i> Alerta!</h5>
                    {{ $message }}
                  </div>
                  @enderror      
                </div>
              </div>
              <div class="col-6">
                <div class="form-group">
                  <label>DNI</label>
                  <input type="text" class="form-control" id="dni" name="dni" value="{{ $user->dni }}">
                </div>
              </div>
            </div>
            <div class="row">   
              <div class="col-6">
                <div class="form-group">
                  <label for="exampleSelectBorder">Rol de Usuario</label>
                  <select class="custom-select form-control-border" id="role" name="role">
                    <option <?php if (($user->role)== 0){ echo "selected"; } ?> value="0">Administrador</option>
                    <option <?php if (($user->role)== 1){ echo "selected"; } ?> value="1">Usuario</option>
                  </select>
                  @error('role')
                  <div class="alert alert-warning alert-dismissible mt-2">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h5><i class="icon fas fa-exclamation-triangle"></i> Alerta!</h5>
                    {{ $message }}
                  </div>
                  @enderror
                </div>
              </div>
              <div class="col-6">
                <div class="form-group">
                  <label>Tel&eacute;fono</label>
                  <input type="text" class="form-control" id="phone" name="phone" value="{{ $user->phone }}">
                </div>
              </div>
            </div>
            <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" class="form-control" id="email" name="email" value="{{ $user->email }}">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Direcci&oacute;n</label>
                                    <input type="text" class="form-control" id="address" name="address" value="{{ $user->address }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                <label for="exampleInputPassword1">Contraseña</label>
                                <input type="password" class="form-control" id="Password1" name="Password1">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                <label for="exampleInputPassword1">Confirmar Contraseña</label>
                                <input type="password" class="form-control" id="Password2" name="Password2">
                                </div>
                            </div>    
                        </div>
                        <div class="row">
                            <div class="col-sm col-md-6 col-lg-4">
                                <img id="img-mostrar" src="/dist/img/{{ $user->photo_profile }}" class="img-fluid" style="max-height:300px;">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="exampleInputFile">Foto</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="photo_profile" name="photo_profile">
                                            <label class="custom-file-label" for="exampleInputFile">Escoger el archivo</label>
                                        </div>
                                        @error('photo_profile')
                                        <div class="alert alert-warning alert-dismissible mt-2">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <h5><i class="icon fas fa-exclamation-triangle"></i> Alerta!</h5>
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
          <button  type="submit" class="btn btn-success">Guardar</button>
          </form>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
  
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
$(document).ready(function(e){
    $('#photo_profile').change(function(){
        let reader = new FileReader();
        reader.onload = (e) => {
            $('#img-mostrar').attr('src',e.target.result);
        }
        reader.readAsDataURL(this.files[0]);
    });
});
</script>
@endsection