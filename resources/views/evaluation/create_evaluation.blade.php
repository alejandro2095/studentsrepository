{{-- Aqui se coloca la plantilla del panel de administrador --}}
@extends('layouts.main')

{{-- Aqui se coloca el titulo de esta pagina que se esta desarroollando --}}
@section('title', 'Crear Evaluacion de Practicante - Administración')

{{-- Aqui la seccion que trae el contenido centrada a la plantilla --}}
@section('section')
<div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">
                Crear Evaluación
            </h3>
            <a type="button" class="btn btn-primary float-right" href="/intern/mostrar_evaluacion/{{$id_intern}}"> Regresar</a>
          </div>
        
          <div class="card-body">
           {{-- Formulario Para guardar la Gerencia --}}
           <form action="{{ url("/intern/mostrar_evaluacion/".$id_intern."/store") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">   
          <div class="col-6">
            <div class="form-group">
              <label>Descripción del documento</label>
              <input type="text" class="form-control" id="description" name="description">
              @error('description')
              <div class="alert alert-warning alert-dismissible mt-2">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h5><i class="icon fas fa-exclamation-triangle"></i> Alerta!</h5>
                {{ $message }}
              </div>
              @enderror      
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
                <label>Estado</label>
                <select class="form-control" id="status" name="status">
                  <option value="1">Aprobado</option>
                  <option value="2">Reprobado</option>
                </select>    
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
                <label>Corrección</label>
                <select class="form-control" id="correction" name="correction">
                  <option value="Culminado">Culminado</option>
                  <option value="Pendiente">Pendiente</option>
                </select>    
            </div>
          </div>
          <div class="col-6">
            <div class="form-group">
              <label>Nota</label>
              <input type="text" class="form-control" id="grade" name="grade">
              @error('grade')
              <div class="alert alert-warning alert-dismissible mt-2">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h5><i class="icon fas fa-exclamation-triangle"></i> Alerta!</h5>
                {{ $message }}
              </div>
              @enderror      
            </div>
          </div>
          <div class="col-md-6 col-lg-4">
            <div class="form-group">
              <label for="customFile">Archivo</label>
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="file" name="file">
                <label class="custom-file-label" for="customFile">Escoje el archivo</label>
                @error('file')
                <div class="alert alert-warning alert-dismissible mt-2">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fas fa-exclamation-triangle"></i> Alerta!</h5>
                  @php
                      echo html_entity_decode($message)
                  @endphp
                </div>
                @enderror 
              </div>
            </div>
          </div>
         
        
          </div>
          <button  type="submit" class="btn btn-success">Guardar</button>
          </form>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>

@endsection
