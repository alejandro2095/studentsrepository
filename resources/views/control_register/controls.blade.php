{{-- Aqui se coloca la plantilla del panel de administrador --}}
@extends('layouts.main')

{{-- Aqui se coloca el titulo de esta pagina que se esta desarroollando --}}
@section('title', 'Control de Registro - Administración')

{{-- Aqui la seccion que trae el contenido centrada a la plantilla --}}
@section('section')
<div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">
                Administraci&oacute;n de Control de Registro
            </h3>
            <a type="button" class="btn btn-success float-right" href="{{ route('control_register.create') }}"><i class="fas fa-plus"></i> Agregar</a>
          </div>
         
          <!-- /.card-header -->
          {{-- Lista de Gerencias --}}
          <div class="card-body">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h5><i class="icon fas fa-check"></i> Alerta!</h5>
              <p>{{ $message }}</p>
            </div>
            @endif
            <table id="example1" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>Practicante</th>
                <th>Gerencia</th>
                <th>Descripción</th>
                <th>Fecha Inicio</th>
                <th>Fecha Final</th>
                <th>Horario</th>
                <th>Horas de Practica</th>
                <th>Acciones</th>
              </tr>
              </thead>
              <tbody>
                @foreach ($controls as $control)
                <tr>
                  @foreach ($interns as $intern)
                      @if ($intern->id==$control->id_intern)
                      <td>{{$intern->name}} {{$intern->lastname}}
                      </td>
                      @endif
                  @endforeach
                  @foreach ($managers as $manager)
                  @if ($manager->id==$control->id_manager)
                  <td>{{$manager->manager}}
                  </td>
                  @endif
              @endforeach
                <td>{{$control->description}}
                </td>
                <td>{{$control->fecha_inicio}}
                </td>
                <td>{{$control->fecha_final}}
                </td>
                <td>{{$control->horario}}
                </td>
                <td>{{$control->horas_practica}}
                </td>
                <td><div class="btn-group">
                    <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" data-offset="-52" aria-expanded="true">
                      <i class="fas fa-bars"></i>
                    </button>
                    <div class="dropdown-menu" role="menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-52px, 31px, 0px);">
                      <form action="{{ url('/control_register/'.$control->id.'/destroy') }}" method="Post">
                        <a href="{{ route('control_register.edit',$control->id) }}" class="dropdown-item"> <i class="fas fa-bars"></i> Editar</a>
                        @csrf
                      
                        <button  type="submit" class="dropdown-item"> <i class="fas fa-bars"></i> Eliminar</button> 
                      </form>
                     </div>
                  </div></td>
              </tr>
                @endforeach
              </tbody>
              <tfoot>
              <tr>
                <th>Practicante</th>
                <th>Gerencia</th>
                <th>Descripción</th>
                <th>Fecha Inicio</th>
                <th>Fecha Final</th>
                <th>Horario</th>
                <th>Horas de Practica</th>
                <th>Acciones</th>
              </tr>
              </tfoot>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
@endsection


