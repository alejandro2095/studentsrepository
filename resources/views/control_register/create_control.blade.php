{{-- Aqui se coloca la plantilla del panel de administrador --}}
@extends('layouts.main')

{{-- Aqui se coloca el titulo de esta pagina que se esta desarroollando --}}
@section('title', 'Crear Control de Registro - Administración')

{{-- Aqui la seccion que trae el contenido centrada a la plantilla --}}
@section('section')
<div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">
                Crear Control de Registro
            </h3>
            <a type="button" class="btn btn-primary float-right" href="{{ route('control_register.index') }}"> Regresar</a>
          </div>
        
          <div class="card-body">
           {{-- Formulario Para guardar la Gerencia --}}
           <form action="{{ route('control_register.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">   
              <div class="col-md-6">
                <div class="form-group">
                    <label>Nombre del Practicante</label>
                    <select class="form-control" id="id_intern" name="id_intern">
                      @foreach ($interns as $intern)
                          @if ($intern->status == 2)
                          <option value="{{$intern->id}}">{{$intern->name }} {{$intern->lastname}}</option>
                          @endif
                      @endforeach
                    </select>    
                </div>
              </div>
          <div class="col-6">
            <div class="form-group">
              <label>Descripción</label>
              <input type="text" class="form-control" id="description" name="description">
              @error('description')
              <div class="alert alert-warning alert-dismissible mt-2">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h5><i class="icon fas fa-exclamation-triangle"></i> Alerta!</h5>
                {{ $message }}
              </div>
              @enderror      
            </div>
          </div>

          <div class="col-6">
            <div class="form-group">
              <label>Fecha inicial</label>
                    <div class="input-group date" id="fecha_inicial" name="fecha_inicial" data-target-input="nearest">
                        <input type="text" class="form-control datetimepicker-input" id="fecha_inicial_control" name="fecha_inicial_control"  data-target="#fecha_inicial"/>
                        <div class="input-group-append" data-target="#fecha_inicial" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                    </div>
              @error('fecha_inicial_control')
              <div class="alert alert-warning alert-dismissible mt-2">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h5><i class="icon fas fa-exclamation-triangle"></i> Alerta!</h5>
                {{ $message }}
              </div>
              @enderror      
            </div>
          </div>

          <div class="col-6">
            <div class="form-group">
              <label>Fecha Final</label>
                    <div class="input-group date" id="fecha_final" name="fecha_final" data-target-input="nearest">
                        <input type="text" class="form-control datetimepicker-input" id="fecha_final_control" name="fecha_final_control"  data-target="#fecha_final"/>
                        <div class="input-group-append" data-target="#fecha_final" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                    </div>
              @error('fecha_final_control')
              <div class="alert alert-warning alert-dismissible mt-2">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h5><i class="icon fas fa-exclamation-triangle"></i> Alerta!</h5>
                {{ $message }}
              </div>
              @enderror      
            </div>
          </div>

          <div class="col-6">
            <div class="form-group">
              <label>Horario</label>
              <input type="text" class="form-control" id="horario" name="horario">
              @error('horario')
              <div class="alert alert-warning alert-dismissible mt-2">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h5><i class="icon fas fa-exclamation-triangle"></i> Alerta!</h5>
                {{ $message }}
              </div>
              @enderror      
            </div>
          </div>

          <div class="col-6">
            <div class="form-group">
              <label>Horas de Practica</label>
              <input type="text" class="form-control" id="horas_practica" name="horas_practica">
              @error('horas_practica')
              <div class="alert alert-warning alert-dismissible mt-2">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h5><i class="icon fas fa-exclamation-triangle"></i> Alerta!</h5>
                {{ $message }}
              </div>
              @enderror      
            </div>
          </div>
        
          </div>
          <button  type="submit" class="btn btn-success">Guardar</button>
          </form>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>

@endsection
