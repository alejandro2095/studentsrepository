@extends('layouts.main')

@section('title', 'Solicitudes de Practicantes - Administración')

@section('section')
<div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">
                Administraci&oacute;n para Solicitudes de Practicantes
            </h3>
         </div>
         
          <!-- /.card-header -->
          {{-- Lista de Gerencias --}}
          <div class="card-body">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h5><i class="icon fas fa-check"></i> Alerta!</h5>
              <p>{{ $message }}</p>
            </div>
            @endif
            <table id="example1" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>Nombre de Practicante</th>
                <th>Gerencia</th>
                <th>Carrera</th>
                <th>Estado</th>
                <th>Acciones</th>
              </tr>
              </thead>
              <tbody>
                @foreach ($interns as $intern)
                @if ($intern->status == 1)
                <tr>
                  <td>{{$intern->name}} {{$intern->lastname}} 
                  </td>
                  @foreach ($managers as $manager)
                    @if ($manager->id == $intern->manager_id)
                    <td>{{ $manager->manager }}
                    </td>
                    @endif
                  @endforeach
                  <td>{{$intern->career}}
                  </td>
                  <td>
                    <p class="badge bg-warning p-3">Sin aprobar</p>
                  </td>
                  <td>
                    <div class="btn-group">
                      <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" data-offset="-52" aria-expanded="true">
                        <i class="fas fa-bars"></i>
                      </button>
                      <div class="dropdown-menu" role="menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-52px, 31px, 0px);">
                        <form action="{{ url('/intern/'.$intern->id.'/destroy') }}" method="Post">
                          <a href="{{ route('intern.edit',$intern->id) }}" class="dropdown-item"> <i class="fas fa-bars"></i> Aprobar</a>
                          @csrf
                          <button  type="submit" class="dropdown-item"> <i class="fas fa-eraser"></i> Rechazar</button> 
                        </form>
                      </div>
                    </div>
                  </td>
                </tr>
                @endif
                @endforeach
              </tbody>
              <tfoot>
              <tr>
                <th>Nombre de Practicante</th>
                <th>Gerencia</th>
                <th>Carrera</th>
                <th>Estado</th>
                <th>Acciones</th>
              </tr>
              </tfoot>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
@endsection


