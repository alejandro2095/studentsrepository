<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ManagerController;
use App\Http\Controllers\InternController;
use App\Http\Controllers\ControlController;
use App\Http\Controllers\ApplicationController;
 
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Aqui se colocan las rutas a trabajar en el sitio web */
Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();
Route::resource('/intern', InternController::class);
Route::group(['middleware' => 'auth'],function(){
    Route::resource('/users', UserController::class);
    Route::resource('/manager', ManagerController::class);
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/intern/mostrar_evaluacion/{id}',[App\Http\Controllers\EvaluationController::class, 'ShowEvaluation']);
    Route::get('/intern/mostrar_evaluacion/create/{id}',[App\Http\Controllers\EvaluationController::class, 'CreateEvaluation']);
    Route::post('/intern/mostrar_evaluacion/{id}/store',[App\Http\Controllers\EvaluationController::class, 'StoreEvaluation']);
    Route::get('/intern/mostrar_evaluacion/{id}/edit/{id_segundo}',[App\Http\Controllers\EvaluationController::class, 'EditEvaluation']);
    Route::post('/intern/mostrar_evaluacion/{id}/update/{id_segundo}',[App\Http\Controllers\EvaluationController::class, 'UpdateEvaluation']);
    Route::post('/intern/mostrar_evaluacion/{id}/destroy',[App\Http\Controllers\EvaluationController::class, 'DestroyEvaluation']);
    Route::post('/control_register/{id}/update',[App\Http\Controllers\ControlController::class, 'UpdateControl']);
    Route::post('/control_register/{id}/destroy',[App\Http\Controllers\ControlController::class, 'DestroyControl']);
    Route::resource('/control_register', ControlController::class);
    Route::resource('/application_requests', ApplicationController::class); 
    Route::post('/intern/{id}/destroy', [App\Http\Controllers\InternController::class,'destroy']);
});