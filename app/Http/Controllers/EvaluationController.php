<?php

namespace App\Http\Controllers;

use App\Models\Evaluation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use File;

class EvaluationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ShowEvaluation($id_intern){
        $evaluations= Evaluation::where('id_intern', '=', $id_intern)->get();

        $promgradeevaluation = Evaluation::where('id_intern', '=', $id_intern)->avg('grade');

        return view('interns.evaluations',compact('evaluations','id_intern','promgradeevaluation'));


    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function CreateEvaluation($id_intern)
    {
        return view('evaluation.create_evaluation',compact('id_intern'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function StoreEvaluation(Request $request,$id_intern)
    {
        $rules = [
            'description' => 'required',
            'grade' => 'required|integer',
            'file' => 'required|mimes:pdf'
        ];
        
            //Aqui se modifican los mensajes de validacion antes de guardarlos en la base de datos
            $messages = [
                'description.required' => 'Debe ingresar una descripcion del archivo a subir.',
                'grade.required' => 'Debe ingresar la nota de evaluación.',
                'grade.integer' =>'Este campo ingresado solo debe contener digitos, sin guiones(-) o algun otro caracter, ni letras',
                'file.required' => 'Debe adjuntar un archivo y solo que sean en <strong>PDF</strong>',
                'file.mimes' => 'Debe adjuntar archivos solo que sean en <strong>PDF</strong>'
            ];
            
            $this->validate($request, $rules, $messages);

            $evaluation = new Evaluation;
            $evaluation->id_intern = $id_intern;
            $evaluation->description = $request->description;
            $evaluation->status = $request->status;
            $evaluation->correction = $request->correction;
            $evaluation->grade = $request->grade;

            if($request->hasFile('file')){
                $archivo_evaluation_application=$request->file('file');
                $archivo_evaluation_application->move(public_path().'/pdf/evaluaciones/',$archivo_evaluation_application->getClientOriginalName());
                $evaluation->file = $archivo_evaluation_application->getClientOriginalName();
            }

            $evaluation->save();

            return redirect('/intern/mostrar_evaluacion/'.$id_intern)
            ->with('success', $evaluation->description .' ha sido registrado exitosamente.');
      
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Evaluation  $evaluation
     * @return \Illuminate\Http\Response
     */
    public function show(Evaluation $evaluation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Evaluation  $evaluation
     * @return \Illuminate\Http\Response
     */
    public function EditEvaluation($id_intern,$edit_evaluation)
    {
        $idediteval = Evaluation::find($edit_evaluation);

        return view('evaluation.edit_evaluation',compact('id_intern','idediteval'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Evaluation  $evaluation
     * @return \Illuminate\Http\Response
     */
    public function UpdateEvaluation(Request $request, $id_intern,$id_evaluation)
    {
        $rules = [
            'description' => 'required',
            'grade' => 'required|integer',
            'file' => 'mimes:pdf'
        ];
        
            //Aqui se modifican los mensajes de validacion antes de guardarlos en la base de datos
            $messages = [
                'description.required' => 'Debe ingresar una descripcion del archivo a subir.',
                'grade.required' => 'Debe ingresar la nota de evaluación.',
                'grade.integer' =>'Este campo ingresado solo debe contener digitos, sin guiones(-) o algun otro caracter, ni letras',
                'file.mimes' => 'Debe adjuntar archivos solo que sean en <strong>PDF</strong>'
            ];
            
            $this->validate($request, $rules, $messages);

            $evaluation = Evaluation::find($id_evaluation);
            $evaluation->id_intern = $id_intern;
            $evaluation->description = $request->description;
            $evaluation->status = $request->status;
            $evaluation->correction = $request->correction;
            $evaluation->grade = $request->grade;

            if($request->grade!=NULL){
            if($request->hasFile('file')){
                $archivo_evaluation_application=$request->file('file');
                $archivo_evaluation_application->move(public_path().'/pdf/evaluaciones/',$archivo_evaluation_application->getClientOriginalName());
                $evaluation->file = $archivo_evaluation_application->getClientOriginalName();
            }
        }else{

        }

            $evaluation->save();

            return redirect('/intern/mostrar_evaluacion/'.$id_intern)
            ->with('success', $evaluation->description .' ha sido modificado exitosamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Evaluation  $evaluation
     * @return \Illuminate\Http\Response
     */
    public function DestroyEvaluation($id_evaluation)
    {
        $evaluation=Evaluation::find($id_evaluation);
        $intern=Evaluation::find($id_evaluation);
        
        if(File::exists(public_path().'/pdf/evaluaciones/'.$intern->file)){
            File::delete(public_path().'/pdf/evaluaciones/'.$intern->file);
            }
        // Storage::delete(public_path()."/pdf/$intern->description/$intern->file");
        $evaluation->delete();
        return redirect('/intern/mostrar_evaluacion/'.$intern->id_intern)
        ->with('success','La Gerencia ha sido eliminada exitosamente.');
    }
}
