<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {   
        $users = User::all();
        return view('users.users',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create_user');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Aqui se validan los campos antes de guardarlos en la base de datos
        $rules = [
            'user_name' => 'required',
            'role' => 'required',
            'Password2' => 'required',
            'photo_profile' => 'required|image|mimes:jpg,jpeg,png,svg|max:1024'
        ];
        
        //Aqui se modifican los mensajes de validacion antes de guardarlos en la base de datos
        $messages = [
            'user_name.required' => 'Agrega el nombre del usuario.',
            'role.required' => 'Agrega el nombre del usuario',
            'Password2.required' =>'Confirme la contraseña.',
            'photo_profile.required' => 'Confirma que el tipo sea una imagen jpeg, jpg, png o el tamaño del archivo menor a 1MB.'
        ];
            
        $this->validate($request, $rules, $messages);

        $user = new User;
        $user->name = $request->user_name;
        $user->role = $request->role;
        $user->dni = $request->dni;
        $user->phone = $request->phone;
        $user->email = $request->email;
        $user->address = $request->address;
        if($request->Password1 == $request->Password2){
            $user->password = bcrypt($request->Password1);
        }

        if($imagen = $request->file('photo_profile')){
            $ruta = '/dist/img/';
            $imagenuser = $user->name.'.'.$imagen->getClientOriginalExtension();
            $imagen->move($ruta,$imagenuser);
            $user->photo_profile = $imagenuser;
        }else{
            $user->photo_profile = "default.png";
        }

        $user->save();

        return redirect()->route('users.index')
            ->with('success','El usuario ha sido creada exitosamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('users.edit_user',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $user)
    {
        $rules = [
            'user_name' => 'required',
            'role' => 'required',
        ];
        
        $messages = [
            'user_name.required' => 'Agrega el nombre del usuario.',
            'role.required' => 'Agrega el nombre del usuario',
        ];
            
        $this->validate($request, $rules, $messages);

        $user = User::find($user);
        $user->name = $request->user_name;
        $user->role = $request->role;
        $user->dni = $request->dni;
        $user->phone = $request->phone;
        $user->email = $request->email;
        $user->address = $request->address;
        if(!empty($request->Password1)){
            if($request->Password1 == $request->Password2){
                $user->password = bcrypt($request->Password1);
            }
        }

        if($imagen = $request->file('photo_profile')){
            $ruta = '/dist/img/';
            $imagenuser = $user->name.'.'.$imagen->getClientOriginalExtension();
            $imagen->move($ruta,$imagenuser);
            $user->photo_profile = $imagenuser;
        }

        $user->save();

        return redirect()->route('users.index')
            ->with('success','El usuario ha sido creada exitosamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        return redirect()->route('users.index')
        ->with('success','El usuario ha sido eliminado exitosamente.');
    }
}
