<?php

namespace App\Http\Controllers;

use App\Models\Manager;
use Illuminate\Http\Request;

class ManagerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $managers = Manager::all();
        
        return view('manager.manager',compact('managers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('manager.create_manager');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Aqui se validan los campos antes de guardarlos en la base de datos
        $rules = [
            'management_name' => 'required',
            'manager' => 'required',
            'vacancy' => 'required',
        ];
        
            //Aqui se modifican los mensajes de validacion antes de guardarlos en la base de datos
            $messages = [
                'management_name.required' => 'Agrega el nombre de la Gerencia.',
                'manager.required' => 'Agrega el nombre del Gerente',
                'vacancy.required' =>'Agrega la cantidad de vacantes y si no hay vacantes debes de introducir el valor de 0 para poder guardar.'
            ];
            
            $this->validate($request, $rules, $messages);

            $manager = new Manager;
            $manager->management_name = $request->management_name;
            $manager->manager = $request->manager;
            $manager->vacancy = $request->vacancy;
            $manager->save();

            return redirect()->route('manager.index')
            ->with('success','La Gerencia ha sido creada exitosamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Manager  $manager
     * @return \Illuminate\Http\Response
     */
    public function show(Manager $manager)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Manager  $manager
     * @return \Illuminate\Http\Response
     */
    public function edit(Manager $manager)
    {
        return view('manager.edit_manager',compact('manager'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Manager  $manager
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $manager)
    {
         //Aqui se validan los campos antes de guardarlos en la base de datos
         $rules = [
            'management_name' => 'required',
            'manager' => 'required',
            'vacancy' => 'required',
        ];
        
            //Aqui se modifican los mensajes de validacion antes de guardarlos en la base de datos
            $messages = [
                'management_name.required' => 'Agrega el nombre de la Gerencia.',
                'manager.required' => 'Agrega el nombre del Gerente',
                'vacancy.required' =>'Agrega la cantidad de vacantes y si no hay vacantes debes de introducir el valor de 0 para poder guardar.'
            ];
            
            $this->validate($request, $rules, $messages);

            $manager = Manager::find($manager);
            $manager->management_name = $request->management_name;
            $manager->manager = $request->manager;
            $manager->vacancy = $request->vacancy;
            $manager->save();

            return redirect()->route('manager.index')
            ->with('success','La Gerencia ha sido modificada exitosamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Manager  $manager
     * @return \Illuminate\Http\Response
     */
    public function destroy(Manager $manager)
    {
        $manager->delete();
        return redirect()->route('manager.index')
        ->with('success','La Gerencia ha sido eliminada exitosamente.');
    }
}
