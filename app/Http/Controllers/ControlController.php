<?php

namespace App\Http\Controllers;

use App\Models\Control;
use App\Models\Manager;
use App\Models\Intern;
use Illuminate\Http\Request;

class ControlController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $controls = Control::all();
        $interns = Intern::all();
        $managers = Manager::all();
        
        return view('control_register.controls',compact('controls','interns','managers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $controls=Control::all();
        $interns=Intern::all();

        return view('control_register.create_control',compact('controls','interns'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'description' => 'required',
            'fecha_inicial_control' => 'required',
            'fecha_final_control' => 'required',
            'horario' => 'required',
            'horas_practica' => 'required|integer',
        ];
        
            //Aqui se modifican los mensajes de validacion antes de guardarlos en la base de datos
            $messages = [
                'description.required' => 'Debe ingresar una descripcion.',
                'fecha_inicial_control.required' => 'Debe ingresar una fecha inicial.',
                'fecha_final_control.required' => 'Debe ingresar una fecha final.',
                'horario.required' => 'Debe ingresar una horario.',
                'horas_practica.required' => 'Debe ingresar una horas_practica.',
                'horas_practica.integer' =>'Este campo ingresado solo debe contener digitos, sin guiones(-) o algun otro caracter, ni letras'
            ];
            
            $this->validate($request, $rules, $messages);

            $control = new Control;
            $control->description = $request->description;
            $control->id_intern = $request->id_intern;
            
            $interns = Intern::all();
            foreach($interns as $intern){
                if($intern->id=$request->id_intern){
                    $control->id_manager=$intern->manager_id;
                }
            }
          
           
            $control->fecha_inicio = $request->fecha_inicial_control;
            $control->fecha_final = $request->fecha_final_control;
            $control->horario = $request->horario;
            $control->horas_practica = $request->horas_practica;

            $control->save();

            return redirect()->route('control_register.index')
            ->with('success', $control->description .' ha sido registrado exitosamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Control  $control
     * @return \Illuminate\Http\Response
     */
    public function show(Control $control)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Control  $control
     * @return \Illuminate\Http\Response
     */
    public function edit($id_control)
    {
        $interns=Intern::all();
        $control=Control::find($id_control);

        return view('control_register.edit_control',compact('control','interns'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Control  $control
     * @return \Illuminate\Http\Response
     */
    public function UpdateControl(Request $request, $id_control)
    {
        $rules = [
            'description' => 'required',
            'fecha_inicial_control' => 'required',
            'fecha_final_control' => 'required',
            'horario' => 'required',
            'horas_practica' => 'required|integer',
        ];
        
            //Aqui se modifican los mensajes de validacion antes de guardarlos en la base de datos
            $messages = [
                'description.required' => 'Debe ingresar una descripcion.',
                'fecha_inicial_control.required' => 'Debe ingresar una fecha inicial.',
                'fecha_final_control.required' => 'Debe ingresar una fecha final.',
                'horario.required' => 'Debe ingresar una horario.',
                'horas_practica.required' => 'Debe ingresar una horas_practica.',
                'horas_practica.integer' =>'Este campo ingresado solo debe contener digitos, sin guiones(-) o algun otro caracter, ni letras'
            ];
            
            $this->validate($request, $rules, $messages);

            $control = Control::find($id_control);
            $control->description = $request->description;
            $control->id_intern = $request->id_intern;
            
            $interns = Intern::all();
            foreach($interns as $intern){
                if($intern->id=$request->id_intern){
                    $control->id_manager=$intern->manager_id;
                }
            }
          
           
            $control->fecha_inicio = $request->fecha_inicial_control;
            $control->fecha_final = $request->fecha_final_control;
            $control->horario = $request->horario;
            $control->horas_practica = $request->horas_practica;

            $control->save();

            return redirect()->route('control_register.index')
            ->with('success', $control->description .' ha sido modificado exitosamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Control  $control
     * @return \Illuminate\Http\Response
     */
    public function destroy(Control $controls)
    {    
        $controls->delete();
     
        return redirect()->route('control_register.index')
        ->with('success','El Control de Registro ha sido eliminado exitosamente.');
    }

    public function DestroyControl($id_control)
    {
        $control=Control::find($id_control);
        // Storage::delete(public_path()."/pdf/$intern->description/$intern->file");
        $control->delete();
        return redirect()->route('control_register.index')
        ->with('success','El Control de Registro ha sido eliminado exitosamente.');
    }

    
}
