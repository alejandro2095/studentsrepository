<?php

namespace App\Http\Controllers;

use App\Models\Intern;
use App\Models\Manager;
use Illuminate\Http\Request;

class ApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $interns = Intern::all();
        $managers = Manager::all();

        return view('application.application',compact('interns','managers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $count_manager = Manager::all()->sum('vacancy');
        $managers = Manager::where("vacancy",">",0)->get();

        return view('interns.create_intern',compact('count_manager','managers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $manager = Manager::find($request->manager_id);
        if($manager->vacancy!=0){
         //Aqui se validan los campos antes de guardarlos en la base de datos
         $rules = [
            'name' => 'required',
            'lastname' => 'required',
            'dni' => 'required|min:13|numeric|unique:intern',
            'email' => 'required|unique:intern',
            'career' => 'required',
            'institution_name' => 'required',
            'number_months_internship' => 'required|integer',
            'internship_application' => 'mimes:pdf',
            'curriculum_vitae' => 'mimes:pdf',
            'covid19_exam' => 'mimes:pdf'
        ];
        
            //Aqui se modifican los mensajes de validacion antes de guardarlos en la base de datos
            $messages = [
                'name.required' => 'Ingresa tu nombre',
                'lastname.required' => 'Ingresa tu apellido',
                'dni.required' =>'Ingresa tu DNI(número de identidad)',
                'dni.min' =>'El DNI ingresado debe contener 13 digitos, sin guiones(-) o algun otro caracter, ni letras',
                'dni.numeric' =>'El DNI ingresado debe contener solo digitos minimo (13), sin guiones(-) o algun otro caracter, ni letras',
                'dni.unique' =>'Este DNI ya esta ingresado en el sistema, porfavor verifique si el DNI ingresado es el correcto.',
                'email.required' => 'Ingresa tu correo electronico',
                'email.unique' =>'Este correo electronico ya esta ingresado en el sistema, porfavor verifique si el correo electronico ingresado es el correcto.',
                'career.required' => 'Ingresa tu carrera',
                'institution_name.required' => 'Ingresa el nombre de la institución',
                'number_months_internship.required' => 'Ingresa la cantidad de meses que durara la Práctica',
                'number_months_internship.integer' =>'Este campo ingresado solo debe contener digitos, sin guiones(-) o algun otro caracter, ni letras',
                'internship_application.mimes' => 'Debe adjuntar archivos solo que sean en <strong>PDF</strong>',
                'curriculum_vitae.mimes' => 'Debe adjuntar archivos solo que sean en <strong>PDF</strong>',
                'covid19_exam.mimes' => 'Debe adjuntar archivos solo que sean en <strong>PDF</strong>'
            ];
            
            $this->validate($request, $rules, $messages);

            $intern = new Intern;
            $intern->name = $request->name;
            $intern->lastname = $request->lastname;
            $intern->dni = $request->dni;
            $intern->phone = $request->phone;
            $intern->email = $request->email;
            $intern->place_residence = $request->place_residence;
            $intern->career = $request->career;
            $intern->status = '1';
            $intern->university = $request->university;
            $intern->institution_name = $request->institution_name;
            $intern->manager_id = $request->manager_id;
            $intern->internship_month = $request->internship_month;
            $intern->number_months_internship = $request->number_months_internship;

            if($request->hasFile('internship_application')){
                $archivo_internship_application=$request->file('internship_application');
                $archivo_internship_application->move(public_path().'/pdf/'.$intern->dni.'/',$archivo_internship_application->getClientOriginalName());
                $intern->internship_month = $archivo_internship_application->getClientOriginalName();
                $intern->internship_application = $archivo_internship_application->getClientOriginalName();
            }

            if($request->hasFile('curriculum_vitae')){
                $archivo_curriculum_vitae=$request->file('curriculum_vitae');
                $archivo_curriculum_vitae->move(public_path().'/pdf/'.$intern->dni.'/',$archivo_curriculum_vitae->getClientOriginalName());
                $intern->internship_month = $archivo_curriculum_vitae->getClientOriginalName();
                $intern->curriculum_vitae = $archivo_curriculum_vitae->getClientOriginalName();
            }

            if($request->hasFile('covid19_exam')){
                $archivo_covid19_exam=$request->file('covid19_exam');
                $archivo_covid19_exam->move(public_path().'/pdf/'.$intern->dni.'/',$archivo_covid19_exam->getClientOriginalName());
                $intern->internship_month = $archivo_covid19_exam->getClientOriginalName();
                $intern->covid19_exam = $archivo_covid19_exam->getClientOriginalName();
            }

            $intern->name = $request->name;
            $intern->save();

            
            $manager->vacancy = $manager->vacancy - 1;
            $manager->save();

            return redirect()->route('intern.create')
            ->with('success', $intern->name . ' '.$intern->lastname .' has sido registrado exitosamente. Se le notificara por medio de correo eléctronico si su solicitud ha sido aprobada o rechazada.');
        }else{
            return redirect()->route('intern.create')
            ->with('errores',' Lamentamos informar que en esta gerencia ya no se encuentran vacantes disponibles por favor vuelva a intentarlo nuevamente con una gerencia disponible.');
        }
        }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Intern  $intern
     * @return \Illuminate\Http\Response
     */
    public function show(Intern $intern)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Intern  $intern
     * @return \Illuminate\Http\Response
     */
    public function edit(Intern $intern)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Intern  $intern
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Intern $intern)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Intern  $intern
     * @return \Illuminate\Http\Response
     */
    public function destroy(Intern $intern)
    {
        //
    }
}
